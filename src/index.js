import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ReactDOM from 'react-dom';
import './styles/app.css';


import Itinerary from './pages/Itinerary';
import Index from './pages/Index';
import Exhibitors from './pages/Exhibitors';
import Venue2 from './pages/Venue2';
import Admin from './pages/Admin';




class App extends Component {

  render() {
    return (
      <Router>
        <div id="container-index">
          <Route exact path="/" component={ Index } />
          <Route exact path="/venues/:venueId/itinerary" component={ Itinerary } />
          <Route exact path="/venues/:venueId" component={ Venue2 } />
          <Route exact path="/venues/:venueId/exhibitors" component={ Exhibitors } />
          <Route path="/admin/" component={ Admin } />
        </div>
      </Router>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
