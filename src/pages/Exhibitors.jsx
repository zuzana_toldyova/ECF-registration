import React, {Component} from 'react';
import NavbarComp from '../components/NavbarComp';
import ExhibitorsContainer from '../components/Exhibitors/ExhibitorsContainer';
import { post  } from '../utils/fetch';

class Exhibitors extends Component {

  constructor(props) {
    super(props);
    this.state = {
      exhibitors: []
    }

    this.getExhibitors.bind(this);
  }

  get venueId() {
    return this.props.match.params.venueId;
  }

  logout = async() => {
    try {
      const response = await post('/users/logout');
      if(response.result) {
        this.goToUrl(`/venues/${this.venueId}`);
      }
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  goToUrl = (url) => {
    this.props.history.push(url);
  }



  getExhibitors(){
    const self = this;
    return fetch(`/exhibitors?venueId=${this.props.match.params.venueId}`)
    .then(function(response) {
      return response.json();
    })
    .then(function(data){
      self.setState({exhibitors: data});
    });
  }

  componentDidMount() {
    this.getExhibitors();
  }

  render(){

    return(
      <div>
        <NavbarComp className="navbar" venueId={this.venueId} goTo={this.goToUrl} logout={this.logout}/>
        <ExhibitorsContainer exhibitors={this.state.exhibitors} />
      </div>
    )
  }
}

export default Exhibitors;
