import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LoginForm from '../components/Venue/LoginForm';
import { get, post } from '../utils/fetch';
import Registration2 from '../components/Venue/Registration2';
import { Carousel } from 'react-bootstrap';
import { validate } from '../utils/validations';

class Venue2 extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: {},
      error: false,
      message: null,
      loginMessage: null,
      showModal: null,
      first: {value: null, isValid: false},
      last: {value: null, isValid: false},
      email: {value: null, isValid: false},
      password: {value: null, isValid: false},
      agree: {value: null, isValid: false},
      firstValidation: null,
      lastValidation: null,
      emailValidation:null,
      passwordValidation: null,
      agreeValidation: null
    }
  }

  //Handle dynamic data to page
  venueId() {
    return this.props.match.params.venueId;
  }

  async getVenuesInfo() {
    try {
      const path = `/venues/${this.venueId()}`;
      const data = await get(path);
      this.setState({ data, error: false});
    }
    catch (ex) {
      this.setState({ error: true });
    }
  }

  //Handle Registration Form Actions
  async handleSubmit() {
    let data = {};
    const arrData = ['first','last', 'email', 'password', 'agree'];
    let valid = true;
    arrData.forEach(name => {
      if (name !== 'agree') {
        data[name] = this.state[name].value;
      }
      if (!this.state[name].isValid){
        valid = false;
        this.setState({
          [name+'Validation']: 'error'
        })
      } else {
        this.setState({
          [name+'Validation']: 'success'
        })
      }
    });

    if (valid){
      try {
        await post(`/users?q=${this.venueId()}`, data);
        this.props.history.push(`/venues/${this.venueId()}/itinerary`);
      }
      catch(ex) {
        this.setState({ message: ex.message})
      }
    }
  }

  handleChange = (e) => {
    const input = e.target.name;
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({
      [input]: { value: value, isValid: validate(e.target) }
    })
  }

  //Handle Login Actions
  open =  e => {
    this.setState({
      showModal: true
    })
  }

  close = e => {
    this.setState({
      showModal: false
    })
  }

  normalSession(){
    return false;
  }

  async handleLogin(data) {
    try {
      let response = await post(`/users/login`, data);
      if (response.result) {
        this.props.history.push(`/venues/${this.venueId()}/itinerary`);
      }
      this.setState({
        loginMessage: response.message
      })
    }
    catch(ex) {
       this.setState({
        loginMessage: ex.message
      })
    }
  }

  componentDidMount() {
    this.getVenuesInfo();
  }

  render(){
    return (
      <div className="venue-container transition-item">
        <div className="row venue-row">
          <div className="col-xs-12  col-sm-6 venue-left-container">
            <img className="rounded brand" alt="" src={require("../../public/ECFLogo.jpg")} />

            <h2> Welcome to {this.state.data.venue_name} ECF - 2017</h2>
            <Carousel className="carousel-venue">
              <Carousel.Item>
                <iframe id="framvid"
                  src={`https://youtube.com/embed/${this.state.data.venue_video}?autoplay=0&controls=0&rel=0&showinfo=0&autohide=1&modestbranding=1&loop=1`}>
                </iframe>
              </Carousel.Item>

              <Carousel.Item>
                <img id="framvid" alt="" src={require("../../public/Stenberg-College-Photo.jpg")} />
              </Carousel.Item>
            </Carousel>
            <h2> Dreams Take of at ECF </h2>

            <h2 className="gold-sponsor"> Gold Sponsors </h2>
            <div className="logo-sponsor">
            {this.state.data.sponsors && this.state.data.sponsors.map(sponsor => (
              <img alt=""className="sponsor-image" key={sponsor.id} src={sponsor.image}/>
              ))}
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 venue-right-container">
            <h4 onClick={this.open}> Login </h4>
            <LoginForm show={this.state.showModal} close={this.close} login={this.handleLogin.bind(this)} message={this.state.loginMessage} admin={this.normalSession()}/>
            <Link to="/venues/1/exhibitors" >
              <h4> Exhibitors </h4>
            </Link>
            <Registration2
            submit={this.handleSubmit.bind(this)}
            message={this.state.message}
            change={this.handleChange}
            firstValidation={this.state.firstValidation}
            lastValidation={this.state.lastValidation}
            emailValidation={this.state.emailValidation}
            passwordValidation={this.state.passwordValidation}
            agreeValidation={this.state.agreeValidation}
            />
          </div>
        </div>
      </div>
      );

  }
}

export default Venue2;