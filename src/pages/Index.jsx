import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import moment from 'moment';
import {Link} from 'react-router-dom';
import _ from 'lodash';


class Index extends Component {
    constructor(props) {
    super(props);
    this.state = {
      venues: [],
      selected: ''
    }
    this.handleChoose.bind(this);
    this.getVenues.bind(this);
  }

  handleChoose(event) {

  }

  getVenues = () => {
    return fetch('/venues')
    .then((response) => {
      return response.json();
    }).then((data) => {
      this.setState({venues: data});
    });
  }

  componentDidMount() {
    this.getVenues();
  }

  render() {
    const chunked = _.chunk([null].concat(this.state.venues), 3);
    return (
      <div className="page-wrapper">
        {chunked.map((chunk, index) =>
          {
            return <div className="row row-style" key={index}>
              {chunk.map((venue, index) =>
                {
                if (venue === null){
                  return (
                    <div className="col-xs-12 col-sm-4 logo" key='logo'>
                      <img className="rounded" alt="" src={require("../../public/ECFLogo.jpg")} />
                      <h3 className="first-title"> Please select which venue you would like to attend </h3>
                    </div>)
                } else {
                  return (
                    <Link to={`/venues/${venue.id}`} key={venue.id}>
                      <div className={`col-xs-12 col-sm-4 item${this.state.venues.indexOf(venue)+1}`} key={venue.id}>
                        <img className="rounded" alt="" src={require(`../../public/${venue.name}.png`)} />
                        <h3 className="city-name"> {venue.name} </h3>
                        <h4> {moment(venue.opening_date).format('LL')} </h4>
                      </div>
                    </Link>)
                  }
                })
              }
            </div>
          })
        }
      </div>
    );
  }
}

export default Index;
