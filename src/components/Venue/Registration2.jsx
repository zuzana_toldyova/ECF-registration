import React, { Component } from 'react';
import { Button, Checkbox, Form,
  FormGroup, FormControl, ControlLabel,
  HelpBlock } from 'react-bootstrap';
import TermsModal from './TermsModal.jsx';


class Registration2 extends Component {
  constructor(props){
    super(props);
    this.state = {
      showPdfModal: null,
    }
  }

  handleChange = (e) => {
    this.props.change(e);
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.submit();
  }

  open =  e => {
    this.setState({
      showPdfModal: true
    })
  }

  close = e => {
    this.setState({
      showPdfModal: false
    })
  }


  render (){
    return(
      <div className="reg-form-container">
        <h2 className="registration-title"> Registration </h2>
        <Form horizontal id="userReg"  onSubmit={this.handleSubmit}>
          <FormGroup   controlId="formHorizontalFirstName" validationState={this.props.firstValidation} onChange={this.handleChange}>
            <ControlLabel className="form-label"> <span className="glyphicon glyphicon-user"></span> First Name</ControlLabel>
            <FormControl className="form-input" name="first" type="text" placeholder="Enter your first name" />
            <FormControl.Feedback />
            {(this.props.firstValidation === 'error') ? <HelpBlock className="error-message"> Name must contain just letters </HelpBlock> :  null }
          </FormGroup>

          <FormGroup validationState={this.props.passwordValidation} name="last" controlId="formHorizontalLastName" onChange={this.handleChange}>
            <ControlLabel className="form-label"> <span className="glyphicon glyphicon-user"></span> Last Name</ControlLabel>
            <FormControl className="form-input" name="last" type="text" placeholder="Last Name" />
            <FormControl.Feedback />
            {(this.props.lastValidation === 'error') ? <HelpBlock> Name must contain just letters </HelpBlock> : null}
          </FormGroup>

          <FormGroup validationState={this.props.emailValidation} name="email" controlId="formHorizontalEmail" onChange={this.handleChange}>
            <ControlLabel className="form-label"> <span className="glyphicon glyphicon-envelope"></span> Email</ControlLabel>
            <FormControl className="form-input" name="email" type="email" placeholder="Email" />
            <FormControl.Feedback />
            {(this.props.emailValidation === 'error') ? <HelpBlock> Email must be this format: ex@ex.com </HelpBlock> : null}
          </FormGroup>

          <FormGroup validationState={this.props.passwordValidation}  controlId="formHorizontalPassword" onChange={this.handleChange}>
            <ControlLabel className="form-label"> <span className="glyphicon glyphicon-lock"></span> Password </ControlLabel>
            <FormControl.Feedback />
            <FormControl className="form-input" name="password" type="password" placeholder="*******" />
            {(this.props.passwordValidation === 'error') ? <HelpBlock> Minimum length 6 characters </HelpBlock> : null}
          </FormGroup>

          <FormGroup validationState={this.props.agreeValidation} onChange={this.handleChange}>
            <Checkbox className="form-label" name="agree">I agree with <a onClick={this.open} herf='ECF_Terms_&_Condtions.pdf'> Terms & Conditions</a></Checkbox>
                    <TermsModal show={this.state.showPdfModal} close={this.close} />
            {(this.props.agreeValidation === 'error') ? <HelpBlock> You must agree with Terms </HelpBlock> : null}
          </FormGroup>

          <FormGroup>
            <Button className="regButton" type="submit">
              Sign Up
            </Button>
          </FormGroup>
        </Form>
        {(this.props.message) ?
            <p className="regError"> {this.props.message} </p>: null
          }
      </div>
      );
  }
}

export default Registration2;



