import React, { Component } from 'react';
import { Col, Row, Thumbnail,Carousel } from 'react-bootstrap';


class Presentation extends Component {

  render() {
    const { venue_name, venue_video, sponsors } = this.props;

    return(

        <Col className="content " xsPush={0} xs={12} smPull={5} sm={7} >
           <Col className="home-inner" xsPush={0} xs={12} smPull={0} sm={10} >
            <Row className="title ">
              <h1> Welcome to ECF {venue_name} 2017 </h1>
            </Row>
            <Col className="video-container" xsOffset={2} xs={10} sm={12} >
              <Carousel>
                <Carousel.Item>
                  <iframe id="framvid"
                    src={`https://youtube.com/embed/${venue_video}?autoplay=0&controls=0&rel=0&showinfo=0&autohide=1&modestbranding=1&loop=1`}>
                  </iframe>
                </Carousel.Item>

                <Carousel.Item>
                  <img id="framvid" alt="" src={require("../../public/Stenberg-College-Photo.jpg")} />
                </Carousel.Item>
              </Carousel>
            </Col>
              <h2> Gold Sponsors </h2>
            <Col className="banner" sm={12}>
              {sponsors && sponsors.map(sponsor => (
                <Col className="sponsor" xs={3}  key={sponsor.id} >
                  <img className="sponsor-image" src={sponsor.image}/>
                </Col>
              ))}
            </Col>
          </Col>
        </Col>

      );
  }
}


export default Presentation;

