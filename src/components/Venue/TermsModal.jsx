import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';
import PDF from 'react-pdf-js';
class TermsModal extends Component {

  get close () {
    return e => {
      this.props.close()
    }
  }

  onDocumentLoad({ total }) {
        this.setState({ total });
    }

    onPageLoad({ pageIndex, pageNumber }) {
        this.setState({ pageIndex, pageNumber });
    }

  render(){


    return(
      <Modal bsSize="large" show={this.props.show} onHide={this.close}>
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body className="modal-terms" >
          <PDF className="pdf" file="/ECF_Terms_&_Condtions.pdf"/>
        </Modal.Body>
      </Modal>
    )
  }
}

export default TermsModal;
