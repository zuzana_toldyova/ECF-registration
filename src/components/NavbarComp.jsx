import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class Navbarcomp extends Component {

  handleClick(url) {
    return () => {
      this.props.goTo(url);
    }
  }

  open = () => {
      this.props.open()
  }

  render() {
    return(
        <header>
          <Navbar inverse collapseOnSelect >
            <Navbar.Header>
              <Navbar.Brand>
                <a href={`/venues/${this.props.venueId}`}  >
                <img alt="logo" src={require("../../public/ECFLogo.jpg")} />
                </a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav pullRight>
                <NavItem className="links" eventKey={1} onClick={this.handleClick(`/venues/${this.props.venueId}/exhibitors`)} >Exhibitors</NavItem>
                <NavItem className="links" eventKey={2} onClick={this.props.logout}> Logout</NavItem>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </header>
      );
  }
}

export default Navbarcomp;