import React, {Component} from 'react';
import Pick from './Pick';
import {ListGroup} from 'react-bootstrap';

class PickedContainer extends Component {

  render(){

    const picked = this.props.picked.map((pick) => {
      return <Pick key={pick.id} { ...pick } onDelete={this.props.onDelete}/>
    });

    return(
      <ListGroup>
        { picked }
      </ListGroup>
    )
  }
}

export default PickedContainer;