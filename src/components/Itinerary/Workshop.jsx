import React, {Component} from 'react';

class Workshop extends Component {

  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }

  handleClick = (e) => {
    e.preventDefault();
    this.props.onPick({
      name: this.props.name,
      id: this.props.id,
      location: this.props.boothLocation,
      workshop: this.props.workshop,
      seminar: this.props.seminar,
    });
  }

  render(){
    return(
      <div>
        <li className="list-align list-group-item adding" onClick={this.handleClick}>
          {this.props.name}
          <i className="fa fa-plus-square-o fa-fw" aria-hidden="true"></i>
        </li>
      </div>
    )
  }
}

export default Workshop;