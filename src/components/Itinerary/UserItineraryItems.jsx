import React, {Component} from 'react';

class UserItineraryItems extends Component {

  get className() {
    let c = "list-align list-group-item";
    if(this.props.workshop || this.props.seminar) {
      c += " workshop";
    }
    return c;
  }

  render() {
    return (
      <li className={this.className}>
        {this.props.name}
        {(this.props.workshop || this.props.seminar) &&
        <i>workshop</i>}
      </li>
    );
  }
}

export default UserItineraryItems;
