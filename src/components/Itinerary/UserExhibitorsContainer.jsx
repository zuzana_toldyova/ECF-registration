import React, {Component} from 'react';
import { ListGroup } from 'react-bootstrap';

import UserExhibitorsItems from './UserExhibitorsItems';


class UserExhibitorsContainer extends Component {



  render() {

    return (

      <ListGroup className="over-flowing">
          {this.props.exhibitors.map(iti => {
            return (
             <UserExhibitorsItems
              key={iti.id}
              name={iti.name}
              id={iti.id}
              location={iti.boothLocation}
              onpick={this.props.onpick}
             />
            );
          })}
      </ListGroup>

    );
  }
}

export default UserExhibitorsContainer;
