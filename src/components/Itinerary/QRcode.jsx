import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';
import QRCode from 'qrcode.react';
class MyQR extends Component {

  get close () {
    return e => {
      this.props.close()
    }
  }

  render(){


    return(
      <Modal show={this.props.show} onHide={this.close}>
        <Modal.Header closeButton>
          <Modal.Title id="modal-QR-title">This QR code contains your full name and email</Modal.Title>
        </Modal.Header >
        <Modal.Body className="modal-QR-body" >
          <QRCode className="qr" value={this.props.user} size={250} />
        </Modal.Body>
      </Modal>
    )
  }
}

export default MyQR;
