import React, { Component } from 'react';
import {Form, FormGroup, Checkbox, ControlLabel,
        FormControl, Button, HelpBlock} from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';


class ExhibitorForm extends Component {
  constructor(props) {
    super(props);

    const { currentExhibitor } = props;

    // temporary empty state in case there is no current exhibitor for the form, that is form is empty
    let temporaryState = {
        name: '',
        description: '',
        url_link: '',
        video_link: '',
        image: '',
        sponsor: false,
        seminar: false,
        workshop: false,
        venues: [],
        tags: [],
      }

    // converting format of currentExhibitor which is an array of exhibitors with the same ID but different venue_ids
    // to temporary state which is single exhibitor with an array of venueIds depending on which venue they're assigned
    if(currentExhibitor) {
      temporaryState = Object.keys(currentExhibitor[0]).reduce((previous, key) => {
        if(key !== 'venue_id' && key !== 'exhibitor_id') {
          previous[key] = currentExhibitor[0][key];
        }
        return previous;
      }, {});
      let venues = [];
      currentExhibitor.forEach(ex => {
        const venue_id = ex.venue_id;
        venues.push(venue_id);
      })
      temporaryState.venues = venues;
    }

    this.state = {
      nameValidation: null,
      venuesValidation: null,
      exhibitor: temporaryState,
      value: this.convertTags(temporaryState.tags),
    }
  }

  // changing formatting of tags from array of ids to array of objects {value: tag.id, label: tag}
  // for react select input to show default values correctly
  convertTags(tagIds) {
    let convertedTags = this.props.allTags.filter(tag => tagIds.includes(tag.id)).map(tag => {
      return {value: tag.id, label: tag.tag}
    })
    return convertedTags;
  }

  // updating state on change for all fields and checkboxes except venues
  handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    const newState = {...this.state.exhibitor};
    newState[name] = value;
    this.setState({exhibitor: newState});
  }

  // updating state on change for venues checkboxes
  handleChangeVenue = (e) => {
    const id = Number(e.target.id);
    const newState = {...this.state.exhibitor};
    if (e.target.checked) {
      newState.venues = newState.venues.concat(id);
    } else {
      let index = newState.venues.indexOf(id);
      newState.venues.splice(index, 1);
    }
    this.setState({exhibitor: newState});
  }

  // validates an input and sends the data to parent admin page submit function
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validateInputs()) {
      let data = this.state.exhibitor;
      data.tags = this.state.value.map(value => {
        return value.value;
      });
      this.props.submit(data);
    }
  }

  // validates an input and sends the data to parent admin page edit function
  handleEdit = (e) => {
    e.preventDefault();
    if (this.validateInputs()) {
      let data = this.state.exhibitor;
      data.tags = this.state.value.map(value => {
        return value.value;
      });
      this.props.edit(data)
    }
  }

  handleDelete = (e) => {
    e.preventDefault();
    this.props.delete();
  }

  // validating name field and venues checkboxes fields
  validateInputs() {
    let isValid = true;
    if (this.state.exhibitor.name.length < 2) {
      this.setState({nameValidation: 'error'});
      isValid = false;
    } else {
      this.setState({nameValidation: null})
    }
    if (!this.state.exhibitor.venues.length) {
      this.setState({venuesValidation: 'error'});
      isValid = false;
    } else {
      this.setState({venuesValidation: null})
    }
    return isValid;
  }

  // getting tags for autocomplete on tags input in the form
  getTags(input) {
    return fetch(`/venues/:id/tags?keyword=${input}`, {
      credentials: 'include'
    })
    .then((response) => { return response.json(); })
    .then((data) => { return {options: data };  });
  }

  // function used for modifying state on tags input change in the form
  onChange = (value) => {
    this.setState({
      value,
    });
  }

  render() {

    const venues = (this.props.venues).map((venue) => {
      return (
        <Checkbox key={venue.id} id={venue.id} name={ venue.name } defaultChecked={this.state.exhibitor.venues.includes(venue.id)} inline>
          { venue.name }
        </Checkbox>
      )
    })

    return (
      <div>
        <Form horizontal id="" autoComplete="on" >

          <FormGroup  controlId="exName" onChange={this.handleChange} validationState={this.state.nameValidation}>
            <ControlLabel>Name</ControlLabel>
            <FormControl name="name" type="text" required placeholder="Exhibitor Name" defaultValue={this.state.exhibitor.name}/>
            <FormControl.Feedback />
            {(this.state.nameValidation === 'error') && <HelpBlock>Name must be at least 2 characters long</HelpBlock>}
          </FormGroup>

          <FormGroup  controlId="exDescription" onChange={this.handleChange}>
            <ControlLabel> Description </ControlLabel>
            <FormControl name="description" type="text" placeholder="Description" defaultValue={this.state.exhibitor.description}/>
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup  controlId="exUrl" onChange={this.handleChange}>
            <ControlLabel> Url Link </ControlLabel>
            <FormControl name="url_link" type="url" placeholder="Url Link: http://" defaultValue={this.state.exhibitor.url_link}/>
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup  controlId="exVideo" onChange={this.handleChange}>
            <ControlLabel> Video Link </ControlLabel>
            <FormControl name="video_link" type="url" placeholder="Video Link: http://" defaultValue={this.state.exhibitor.video_link} />
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup  controlId="exImage" onChange={this.handleChange}>
            <ControlLabel> Image Link </ControlLabel>
            <FormControl name="image" type="url" placeholder="Image Link: http://" defaultValue={this.state.exhibitor.image} />
            <FormControl.Feedback />
          </FormGroup>

           <FormGroup  controlId="tags" data-role="tagsinput" onChange={this.handleTagChange}>
            <ControlLabel> Exhibitors tags </ControlLabel>
            <Select.Async
              multi={true}
              autoload={false}
              loadOptions={this.getTags}
              onChange={this.onChange}
              value={this.state.value}
              name="tags"
              type="text"
              placeholder="insert tags for exhibitor"
            />
            <FormControl.Feedback />
          </FormGroup>

          <FormGroup onChange={this.handleChange}>
            <Checkbox name="sponsor" inline defaultChecked={this.state.exhibitor.sponsor}>
              Sponsor
            </Checkbox >
            {' '}
            <Checkbox name="workshop" inline defaultChecked={this.state.exhibitor.workshop}>
              Workshop
            </Checkbox>
            {' '}
            <Checkbox name="seminar" inline defaultChecked={this.state.exhibitor.seminar}>
              Seminar
            </Checkbox>
          </FormGroup>

          <FormGroup onChange={this.handleChangeVenue} validationState={this.state.venuesValidation}>
            { venues }
            {(this.state.venuesValidation === 'error') && <HelpBlock>Pick at least one venue for the exhibitor</HelpBlock>}
          </FormGroup>

          {this.props.currentExhibitor ? (
            <div>
              <Button onClick={this.handleEdit}> Update </Button>
              <Button onClick={this.handleDelete}> Delete </Button>
            </div>
            ): (
            <Button onClick={this.handleSubmit}> Create </Button>
            )
          }
        </Form>
      </div>
    );
  }
}

export default ExhibitorForm;
