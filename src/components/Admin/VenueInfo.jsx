import React, {Component} from 'react';

class VenueInfo extends Component {

  constructor(props) {
    super(props);
   this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.props.clickOnVenue(this.props.VenueId, this.props.venueName);
  }

  render() {
    return (
      <tr className="clickable" onClick={this.handleClick }>
        <th>{this.props.venueName}</th>
        <th>{this.props.venueReg}</th>
        <th>{this.props.VenueExh}</th>
      </tr>
    );
  }
}

export default VenueInfo;