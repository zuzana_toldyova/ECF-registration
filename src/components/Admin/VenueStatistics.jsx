import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import { get } from '../../utils/fetch';

class VenueStatistics extends Component{
  constructor(props) {
    super(props);
    this.state = {
      config: null
    };
  }

  async getStatisticInfo() {
    try {
      const path = `/venues/${this.props.venueId}/statistics?top=5`;
      const data = await get(path);
      this.setState({
        config: {
          chart: {type: 'column'},
          title: {text: 'Most popular exhibitors'},
          xAxis: {categories: data.name},
          series: [{name: 'Users' ,data: data.count}]
        }
      })
    }
    catch (ex) {
      console.log(ex);
    }
  }

  componentDidMount(){
    this.getStatisticInfo();
  }

  render() {

     return (
      <div>
        {this.state.config &&
        <div className="statistics-container">
          <h2> Venue Statistics </h2>
          <ReactHighcharts config={this.state.config} />
        </div>
        }
      </div>
    )
  }
}

export default VenueStatistics;