import React, {Component} from 'react';
import { Modal } from 'react-bootstrap';
import ExhibitorForm from './ExhibitorForm';
import TagsForm from './TagsForm';

class AddExhibitorForm extends Component {

  get handleClose() {
    return () => {
      this.props.closeModal();
    }
  }

  render() {
    return(
      <div>
        <Modal show={true} onHide={this.props.closeModal}>
          {this.props.tags ?
            <Modal.Body>
              <Modal.Header closeButton >
                <Modal.Title>Tags Form</Modal.Title>
              </Modal.Header>
              <TagsForm
                venues={this.props.venues}
                close={this.handleClose}
                submit={this.props.addTag}
              />
            </Modal.Body>
            :
            <Modal.Body>
              <Modal.Header closeButton >
                <Modal.Title>Exhibitor Form</Modal.Title>
              </Modal.Header>
              <ExhibitorForm
                venues={this.props.venues}
                delete={this.props.deleteExhibitor}
                edit={this.props.editExhibitor}
                submit={this.props.addExhibitor}
                close={this.handleClose}
                currentExhibitor={this.props.currentExhibitor}
                allTags={this.props.allTags}
              />
          </Modal.Body>}
        </Modal>
      </div>
    );
  }
}


export default AddExhibitorForm;
