module.exports = {

  checkLogin: function(req, res, next) {
    if (!req.session.userId) {
      res.status(401).send({error: 'User is not logged in'});
    } else {
      next();
    }
  },

  formatMessage: (error) => {
    let message = '';
    let column = '';
    let match;
    if (error.constraint === undefined){
      switch (error.column) {
      case 'first_name' :
        column = 'First Name';
        break;
      case 'last_name' :
        column = 'Last Name';
        break;
      case 'email':
        column = 'Email';
        break;
      case 'password':
        colum = 'Password';
        break;
      default:
        column = 'Some';
        break;
      }
      return message = `${column} is empty, type a valid entry`;
    } else {
      let regex = /=(.+)/;
      let matches = error.detail.match(regex);
      if (matches){
        match = matches[1];
      }
      return message = `This email ${match}`;
    }
  }

};