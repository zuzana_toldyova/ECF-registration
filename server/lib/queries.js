var _ = require('lodash');
module.exports = (knex) => {
  return {
// querries for venues routes
// landing page:


    getAllVenues: () => {
      return knex.select('id', 'name', 'opening_date')
        .from('venues')
      ;
    },
    // registration page:
    getVenueSponsors: (venueId) => {
      return knex.select('venues.name AS venue_name', 'venues.video_link AS venue_video', 'exhibitors.name AS sponser_name', 'exhibitors.image', 'exhibitors.id AS exhibitors_id'  )
      .from('venues')
      .leftOuterJoin('booths', 'venues.id', 'booths.venue_id')
      .leftOuterJoin('exhibitors', 'booths.exhibitor_id', 'exhibitors.id')
      .where({
        venue_id: venueId,
        sponsor: true,
        workshop: false,
        seminar: false
      })
      .then(exhibitors => exhibitors.reduce((acc, i) => {
        if(!acc.venue_name || acc.venue_name !== i.venue_name) {
          acc = {
            venue_name: i.venue_name,
            venue_video: i.venue_video,
            sponsors: []
          };
        }
        acc.sponsors.push({
          name: i.sponser_name,
          image: i.image,
          id: i.exhibitors_id
        });
        return acc;
      }, {}))
      //.then(done)
      ;
    },

    getAllExhibitors(){
      return knex('exhibitors')
      .join('booths', 'exhibitors.id', 'booths.exhibitor_id')
      .select('exhibitor_id', 'name', 'description', 'url_link', 'video_link', 'sponsor', 'workshop', 'seminar', 'image', 'venue_id');
    },

    getExhibitor(exhibitorId) {
      return knex('exhibitors')
      .join('booths', 'exhibitors.id', 'booths.exhibitor_id')
      .select('exhibitor_id', 'name', 'description', 'url_link', 'video_link', 'sponsor', 'workshop', 'seminar', 'image', 'venue_id')
      .where('exhibitors.id', exhibitorId);
    },

    getVenueExhibitors: (venueId, done) => {
      knex.select('exhibitor_id', 'name', 'description', 'url_link', 'video_link', 'sponsor', 'workshop', 'seminar', 'image', 'venue_id', 'booths.location AS boothLocation')
        .from('exhibitors')
        .leftOuterJoin('booths', 'exhibitors.id', 'booths.exhibitor_id')
        .where({
          venue_id: venueId
        })
        .then(done);
    },


    getExhibitorByTags: (venueId, keyword, done) => {
      knex.select('exhibitors.id AS exhibitor_id', 'exhibitors.name', 'exhibitors.sponsor', 'exhibitors.workshop', 'exhibitors.seminar', 'booths.location AS boothLocation')
        .from('booths')
          .leftOuterJoin('exhibitors', 'booths.exhibitor_id', 'exhibitors.id' )
          .leftOuterJoin('exhibitors_tags', 'exhibitors.id', 'exhibitors_tags.exhibitor_id')
          .leftOuterJoin('tags', 'tags.id', 'exhibitors_tags.tag_id')
        .where({
          venue_id: venueId,
          tag: keyword
        })
        .then(done);

    },

    getTagsId: (tag, done) => {
      knex.select('id')
        .from('tags')
        .where('tag', tag)
        .then(done);
    },

    insertUserTag: (data, done) => {
      knex('users_tags')
      .insert(data)
      .then(done)
      .catch(err => {
        console.log(err);
      });
    },

    deleteItinerary: (data) => {
      return knex('itineraries')
      .where('exhibitor_id', data.exhibitor_id)
      .andWhere('user_id', data.user_id)
      .del();
    },

    insertItinerary: (data, done) => {
      knex('itineraries')
        .insert(data)
        .then(done)
        .catch(err => {
          console.log(err);
        });
    },

    getItinerary: (venueId, userId, done) => {
      return knex.select('exhibitors.name', 'exhibitors.id', 'booths.location', 'exhibitors.workshop', 'exhibitors.seminar')
        .from('itineraries')
        .leftOuterJoin('exhibitors', 'itineraries.exhibitor_id', 'exhibitors.id')
        .leftOuterJoin('booths', 'booths.exhibitor_id', 'exhibitors.id')
        .where({
          venue_id: venueId,
          user_id: userId
        })
        .then(done);
    },

    insertUser: (data) => {
      return knex('users')
        .returning('id')
        .insert(data)
        ;
    },

    getAdminInfo: () => {
      var booths_p = knex.select('venues.name', 'venues.id').count('booths.id AS exh')
        .from('venues')
        .leftOuterJoin('booths', 'venues.id', 'booths.venue_id')
        .groupBy('venues.id');//.then(done);
      var regs_p = knex.select('venues.name').count('registrations.id AS Reg')
        .from('venues')
        .leftOuterJoin('registrations', 'venues.id', 'registrations.venue_id')
        .groupBy('venues.id');//.then(done);
      return Promise
        .all([booths_p, regs_p])
        .then((results) => {

          var booths = results[0];
          var regs = results[1];
          //var merged = _.mergeByKey(booths, regs, 'name');
          var merged = merge_object_arrays (booths, regs, 'name') // start sequence
           // get the value (array) out of the sequence

           function merge_object_arrays (arr1, arr2, match) {
              return _.union(
                _.map(arr1, function (obj1) {
                  var same = _.find(arr2, function (obj2) {
                    return obj1[match] === obj2[match];
                  });
                  return same ? _.extend(obj1, same) : obj1;
                }),
                _.reject(arr2, function (obj2) {
                  return _.find(arr1, function(obj1) {
                    return obj2[match] === obj1[match];
                  });
               })
              );
            }

          return merged;

        });
    },

    insertRegistration: (data) => {
      return knex('registrations')
        .insert(data);
    },

    insertExhibitors(data) {
      return knex('exhibitors')
        .insert(data)
        .returning('id');

    },

    insertExhibitorTags(data) {
      return knex('exhibitors_tags')
      .insert(data)
      .returning('exhibitor_id');
    },

    getExhibitorTags(exhibitorId) {
      return knex('exhibitors_tags')
      .select('tag_id')
      .where('exhibitor_id', exhibitorId);
    },


    deleteAllExhibitorsTags(exhibitorId){
      return knex('exhibitors_tags')
      .where('exhibitor_id', exhibitorId)
      .del();
    },

    insertExhibitorsToVenue(data) {
      return knex('booths')
      .insert(data, 'exhibitor_id');
    },

    updateExhibitor(data, id) {
      return knex('exhibitors')
      .where('id', id)
      .update(data)
      .returning('id');
    },

    deleteExhibitor(id) {
      return knex('exhibitors')
      .where('id', id)
      .del();
    },

    deleteAllBooths(exhibitorId) {
      return knex('booths')
      .where('exhibitor_id', exhibitorId)
      .del();
    },

    getTop5ExhibitorsByVenue(id) {
      return knex('itineraries')
        .select('exhibitors.name')
        .count('itineraries.exhibitor_id')
        .leftJoin('exhibitors', 'itineraries.exhibitor_id', 'exhibitors.id')
        .leftJoin('booths', 'exhibitors.id', 'booths.exhibitor_id')
        .where({'booths.venue_id': id})
        .groupBy('booths.venue_id', 'exhibitors.name', 'itineraries.exhibitor_id')
        .orderBy('count', 'desc')
        .limit(5);
    },

    getUser(data) {
      return knex('users')
        .select('id', 'password')
        .where(data)
        .limit(1);
    },


    findUserInfo(id){
      return knex('users')
        .select('first_name', 'last_name', 'email')
        .where({'users.id': id});
      },

    // tag search page:
    getMatchedTags: (keyword, done) => {
      knex.select('tag', 'id')
        .from('tags')
        .where('tag', 'like', `%${keyword}%`)
        .then(done);

    },

    insertTag(data) {
      return knex('tags')
      .insert(data, 'id');
    },

    getTags() {
      return knex('tags')
      .select('*');
    },

    getUserTags(userId) {
      return knex('users_tags')
      .join('tags', 'users_tags.tag_id', 'tags.id')
      .select('tag_id AS value', 'tag AS label')
      .where('users_tags.user_id', userId);
    }

    // await createUserAndRegistation() {
    //   const t = await knex.transaction();

    //   try
    //   {
    //     ....
    //     await t.commit();
    //   }
    //   catch(ex) {
    //     await t.rollback();
    //     throw ex;
    //   }
    // }

  };
};
