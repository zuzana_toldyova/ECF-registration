"use strict";

const express = require('express');
const router  = express.Router();
const utils = require('../lib/utils');

module.exports = (queries, util) => {

  router.get('/', (req, res) => {
    queries.getTags()
    .then(data => {
      if (!data) {
        return Promise.reject({message: "Couldn't retrieve tags information."});
      }
      res.json(data);
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  router.post('/', (req, res) => {
    const tags = req.body.tags;
    queries.insertTag(tags)
    .then(id => {
      if(!id) {
        return Promise.reject({message: "Tag couldn't be inserted."});
      }
      res.json({inserted: id});
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  router.get('/searches', utils.checkLogin, (req, res) => {
    const userId = req.session.userId;
    queries.getUserTags(userId)
    .then(result => {
      if (!result) {
        return Promise.reject({message: "Something went wrong."});
      }
      res.json({tags: result});
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  return router;
};