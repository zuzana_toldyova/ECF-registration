"use strict";

const utils = require('../lib/utils');

const express = require('express');
const router  = express.Router();

module.exports = (queries, util) => {

  router.get("/", (req, res) => {
    queries.getAllVenues()
    .then((venu) => {
      res.json(venu);
    })
    .catch((error) => {
      console.log("UNKNOWN ERROR", error);
      res.status(500).send();
    });
  });


  router.get("/:id", (req, res) => {
    const venueId = req.params.id;
    queries.getVenueSponsors(venueId)
      .then(venue => {
        if(!venue.venue_name) {
          return res.status(404).end();
        }
        res.json(venue);
      });
  });

  router.get("/:id/statistics", (req, res) => {
    const venueId = req.params.id;
    if(req.query.top === '5') {
      queries.getTop5ExhibitorsByVenue(venueId)
      .then(data => {
        let name = data.map(el => {
          return el.name;
        });
        let count = data.map(el => {
          return Number(el.count);
        });
        let obj = {name, count};
        res.json(obj);
      })
      .catch(ex => {
        res.status(404).send(ex.message);
      });
    }
  });

  router.get("/:id/tags", (req, res) => {
    const tagPartial = req.query.keyword;
    queries.getMatchedTags(tagPartial, (tags) => {
      let result = [];
      for (let tag of tags){
        let option = {
          value: tag.id,
          label: tag.tag
        };
        result.push(option);
      }
      res.json(result);
    });
  });


  // inserting tags that user was searching for
  router.post('/:id/searches', utils.checkLogin, (req, res) => {
    const userId = req.session.userId;
    const tagId = req.body.tagId;
    let data = [];
    data.push({tag_id: tagId, user_id: userId});
    queries.insertUserTag(data, (result) => {
      res.status(200).send();
    });
  });

  router.post('/:id/itinerary', utils.checkLogin, (req, res) => {
    const userId = req.session.userId;
    const itinerary = req.body.itinerary;
    const data = {
      'exhibitor_id': itinerary.id,
      'user_id': userId
    };
    queries.insertItinerary(data, (result) => {
      res.status(200).send();
    });
  });

  router.post('/:id/itinerary/delete', utils.checkLogin, (req, res) => {
    const userId = req.session.userId;
    const itinerary = req.body.itinerary;
    const data = {
      'exhibitor_id': itinerary.id,
      'user_id': userId
    };
    queries.deleteItinerary(data)
    .then( data => {
      res.json(data);
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  router.get('/:id/itinerary', utils.checkLogin, (req, res) => {
    const userId = req.session.userId;
    const venueId = req.params.id;
    queries.getItinerary(venueId, userId, (result) => {
      res.json(result);
    });
  });

  return router;
};
