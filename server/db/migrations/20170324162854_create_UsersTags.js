
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users_tags', function (table) {
    table.increments();
    table.integer('user_id').unsigned();
    table.foreign('user_id').references("users.id").onDelete('cascade');
    table.integer('tag_id').unsigned();
    table.foreign('tag_id').references("tags.id").onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users_tags');
};