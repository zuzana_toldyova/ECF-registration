
exports.up = function(knex, Promise) {
  return knex.schema.table('exhibitors', function (table) {

    table.string('image');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('exhibitors', function (table) {
    table.dropColumn('image')
 });
};