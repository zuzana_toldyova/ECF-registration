
exports.up = function(knex, Promise) {
  return knex.schema.createTable('registrations', function (table) {
    table.increments();
    table.integer('user_id').unsigned()
    table.foreign('user_id').references("users.id").onDelete('cascade');
    table.integer('venue_id').unsigned()
    table.foreign('venue_id').references("venues.id").onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('registrations');
};
