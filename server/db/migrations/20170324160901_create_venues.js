
exports.up = function(knex, Promise) {
  return knex.schema.createTable('venues', function (table) {
    table.increments();
    table.string('name');
    table.string('contact_info');
    table.string('video_link');
    table.date('opening_date');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('venues');
};
