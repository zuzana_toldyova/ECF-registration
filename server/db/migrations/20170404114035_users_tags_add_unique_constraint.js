
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('users_tags', function (table) {
    table.unique(['user_id', 'tag_id']);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('users_tags', function (table) {
    table.dropUnique(['user_id', 'tag_id']);
  });
};
