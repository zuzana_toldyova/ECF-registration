
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('users', function(t) {
    // console.log(t.string('first_name').notNullable());
    t.string('first_name').notNullable().alter();
    t.string('last_name').notNullable().alter();
    t.string('email').notNullable().unique().alter();
    t.string('password').notNullable().alter();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('users', function (t) {
    t.string('first_name').nullable().alter();
    t.string('last_name').nullable().alter();
    t.string('email').nullable().dropUnique().alter();
    t.string('password').nullable().alter();
 });
};
