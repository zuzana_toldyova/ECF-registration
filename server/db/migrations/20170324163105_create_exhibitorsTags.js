
exports.up = function(knex, Promise) {
  return knex.schema.createTable('exhibitors_tags', function (table) {
    table.increments();
    table.integer('exhibitor_id').unsigned()
    table.foreign('exhibitor_id').references("exhibitors.id").onDelete('cascade');
    table.integer('tag_id').unsigned()
    table.foreign('tag_id').references("tags.id").onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('exhibitors_tags');
};