
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('tags', function (table) {
    table.unique('tag');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('tags', function (table) {
    table.dropUnique('tag');
  });
};
