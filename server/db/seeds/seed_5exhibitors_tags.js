
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('exhibitors_tags').del()
    .then(function () {
      return Promise.all([
        // Inserts seed entries
        knex('exhibitors_tags').insert({exhibitor_id: 1, tag_id: 3 }),

        knex('exhibitors_tags').insert({exhibitor_id: 2, tag_id: 3 }),
        knex('exhibitors_tags').insert({exhibitor_id: 2, tag_id: 7 }),

        knex('exhibitors_tags').insert({exhibitor_id: 3, tag_id: 2 }),

        knex('exhibitors_tags').insert({exhibitor_id: 4, tag_id: 1 }),
        knex('exhibitors_tags').insert({exhibitor_id: 4, tag_id: 7 }),

        knex('exhibitors_tags').insert({exhibitor_id: 5, tag_id: 11 }),
        knex('exhibitors_tags').insert({ exhibitor_id: 5, tag_id: 9 }),

        knex('exhibitors_tags').insert({exhibitor_id: 6, tag_id: 2 }),

        knex('exhibitors_tags').insert({exhibitor_id: 7, tag_id: 4 }),
        knex('exhibitors_tags').insert({exhibitor_id: 7, tag_id: 6 }),
        knex('exhibitors_tags').insert({exhibitor_id: 7, tag_id: 8 }),
        knex('exhibitors_tags').insert({exhibitor_id: 7, tag_id: 10 }),

        knex('exhibitors_tags').insert({exhibitor_id: 8, tag_id: 6 }),
        knex('exhibitors_tags').insert({exhibitor_id: 8, tag_id: 9 }),

        knex('exhibitors_tags').insert({exhibitor_id: 9, tag_id: 4 }),
        knex('exhibitors_tags').insert({exhibitor_id: 9, tag_id: 6 }),
        knex('exhibitors_tags').insert({exhibitor_id: 9, tag_id: 8 }),
        knex('exhibitors_tags').insert({exhibitor_id: 9, tag_id: 10 }),

        knex('exhibitors_tags').insert({exhibitor_id: 10, tag_id: 6 }),
        knex('exhibitors_tags').insert({exhibitor_id: 10, tag_id: 8 }),

        knex('exhibitors_tags').insert({exhibitor_id: 11, tag_id: 8 }),
        knex('exhibitors_tags').insert({exhibitor_id: 11, tag_id: 3 }),

        knex('exhibitors_tags').insert({exhibitor_id: 12, tag_id: 10 }),
        knex('exhibitors_tags').insert({exhibitor_id: 12, tag_id: 6 }),

        knex('exhibitors_tags').insert({exhibitor_id: 13, tag_id: 5 }),
        knex('exhibitors_tags').insert({exhibitor_id: 13, tag_id: 6 }),
        // workshop
        knex('exhibitors_tags').insert({exhibitor_id: 14, tag_id: 5 }),
        knex('exhibitors_tags').insert({exhibitor_id: 14, tag_id: 6 }),
        //seminar
        knex('exhibitors_tags').insert({exhibitor_id: 15, tag_id: 2 }),

      ]);
    });
};
