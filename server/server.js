"use strict";


//require('dotenv').config({path: 'server/.env'});

require('dotenv').config({path: __dirname + '/.env'});
var _ = require('lodash');

const PORT        = process.env.PORT || 4000;
const ENV         = process.env.ENV || "development";
const express     = require("express");
const bodyParser  = require("body-parser");
const cookieSession  = require('cookie-session');


const app         = express();

const knexConfig  = require("./knexfile");
const knex        = require("knex")(knexConfig[ENV]);
const morgan      = require('morgan');
const knexLogger  = require('knex-logger');

const queries = require("./lib/queries.js")(knex);
const util = require('./lib/utils.js');
// Seperated Routes for each Resource
const usersRoutes = require("./routes/users")(queries, util);
const adminsRoutes = require("./routes/admins")(queries);
const exhibitorsRoutes = require("./routes/exhibitors")(queries, util);
const venuesRoutes = require("./routes/venues")(queries, util);
const tagsRoutes = require("./routes/tags")(queries, util);

app.use(cookieSession({
  name: 'session',
  keys: [process.env.Secret_Session || `Secret_Session`]
}));


// Load the logger first so all (static) HTTP requests are logged to STDOUT
// 'dev' = Concise output colored by response status for development use.
//         The :status token will be colored red for server error codes, yellow for client error codes, cyan for redirection codes, and uncolored for all other codes.
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// Log knex SQL queries to STDOUT as well
app.use(knexLogger(knex));

app.use(express.static("build"));

// Mount all resource routes
app.use("/users", usersRoutes);
app.use("/tags", tagsRoutes);
app.use("/admins", adminsRoutes);
app.use("/exhibitors", exhibitorsRoutes);
app.use("/venues", venuesRoutes);

// Home page

app.listen(PORT, () => {
  console.log("Example app listening on port " + PORT);
});